<?php

namespace Lemonway\SdkBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;


class LemonwaySdkExtension extends Extension
{


    public function load(array $configs, ContainerBuilder $container)
    {

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('lemonway.directKitUrl', $config['directKitUrl']);
        $container->setParameter('lemonway.webKitUrl', $config['webKitUrl']);
        $container->setParameter('lemonway.login', $config['login']);
        $container->setParameter('lemonway.password', $config['password']);
        $container->setParameter('lemonway.lang', $config['lang']);
        $container->setParameter('lemonway.debug', $config['debug']);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('lemonway.xml');
    }



}
